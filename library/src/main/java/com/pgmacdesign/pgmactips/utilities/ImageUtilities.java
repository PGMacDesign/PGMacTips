package com.pgmacdesign.pgmactips.utilities;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import androidx.annotation.IntRange;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import android.util.Base64;
import android.widget.ImageView;

import com.pgmacdesign.pgmactips.adaptersandlisteners.OnTaskCompleteListener;
import com.pgmacdesign.pgmactips.datamodels.ImageMimeType;
import com.pgmacdesign.pgmactips.misc.CustomAnnotationsBase;
import com.pgmacdesign.pgmactips.misc.PGMacTipsConstants;
import com.pgmacdesign.pgmactips.transformations.CircleTransform;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by pmacdowell on 8/15/2016.
 */
public class ImageUtilities {

    public static final String INVALID_PIXEL_POSITIONS =
            "Invalid pixel positions. Please check the passed params and start again";

    //region Picasso Circular Images With Caching
    /**
     * Set a circular image into a view and set caching.
     *
     * @param urlThumbnail          URL String to use
     * @param viewToSet             View to set it into
     * @param backupImageResourceId Backup resource id in case the String url fails parsing
     * @param <T>                   {T extends View}
     */
    @CustomAnnotationsBase.RequiresDependency(requiresDependency = CustomAnnotationsBase.Dependencies.Picasso)
    public static <T extends ImageView> void setCircularImageWithPicasso(String urlThumbnail,
                                                                         final T viewToSet,
                                                                         final int backupImageResourceId,
                                                                         final Integer circularFrameColor,
                                                                         final Integer circularFrameWidth) {

        if (StringUtilities.isNullOrEmpty(urlThumbnail)) {
            try {
                Picasso.get().load(backupImageResourceId).
                        transform(new CircleTransform(circularFrameColor, circularFrameWidth)).into(viewToSet);

            } catch (Exception e) {
            }
        } else {

            final String innerUrlThumbnail = urlThumbnail;

            try {
                Picasso.get()
                        .load(innerUrlThumbnail)
                        .networkPolicy(NetworkPolicy.OFFLINE)
                        .transform(new CircleTransform(circularFrameColor, circularFrameWidth))
                        .into(viewToSet, new Callback() {
                            @Override
                            public void onSuccess() {
                                //Load the image into cache for next time
                                try {
                                    List<String> toCache = new ArrayList<String>();
                                    toCache.add(innerUrlThumbnail);
                                    ImageUtilities.LoadImagesIntoPicassoCache async = new
                                            ImageUtilities.LoadImagesIntoPicassoCache(toCache);
                                    async.execute();
                                } catch (Exception e2) {
                                }
                            }

                            @Override
                            public void onError(Exception e) {
                                //Can trigger if image not in cache
                                Picasso.get().load(innerUrlThumbnail)
                                        .transform(new CircleTransform(circularFrameColor, circularFrameWidth))
                                        .into(viewToSet, new Callback() {
                                            @Override
                                            public void onSuccess() {
                                                //Load the image into cache for next time
                                                try {
                                                    List<String> toCache = new ArrayList<String>();
                                                    toCache.add(innerUrlThumbnail);
                                                    ImageUtilities.LoadImagesIntoPicassoCache async = new
                                                            ImageUtilities.LoadImagesIntoPicassoCache(toCache);
                                                    async.execute();
                                                } catch (Exception e2) {}
                                            }

                                            @Override
                                            public void onError(Exception e) {
                                                Picasso.get().load(backupImageResourceId)
                                                        .transform(new CircleTransform(circularFrameColor, circularFrameWidth))
                                                        .into(viewToSet);
                                            }
                                        });

                            }
                            
                        });
            } catch (Exception e) {
                try {
                    Picasso.get().load(backupImageResourceId).
                            transform(new CircleTransform(circularFrameColor, circularFrameWidth)).into(viewToSet);
                } catch (Exception e1) {
                }
            }
        }
    }

    /**
     * Set a circular image into a view and set caching. Overloaded to allow for excluding
     * max cache size float
     *
     * @param urlThumbnail          URL String to use
     * @param viewToSet             View to set it into
     * @param backupImageResourceId Backup resource id in case the String url fails parsing
     * @param <T>                   {T extends View}
     */
    @CustomAnnotationsBase.RequiresDependency(requiresDependency = CustomAnnotationsBase.Dependencies.Picasso)
    public static <T extends ImageView> void setCircularImageWithPicasso(String urlThumbnail,
                                                                         final T viewToSet,
                                                                         final int backupImageResourceId) {
        ImageUtilities.setCircularImageWithPicasso(urlThumbnail, viewToSet, backupImageResourceId,
                 null, null);
    }

    //endregion

    //region Picasso Circular Images Without Caching
    /**
     * Set a circular image into a view and set caching. Overloaded to allow for excluding
     * max cache size float
     *
     * @param urlThumbnail          URL String to use
     * @param viewToSet             View to set it into
     * @param backupImageResourceId Backup resource id in case the String url fails parsing
     * @param <T>                   {T extends View}
     */
    @CustomAnnotationsBase.RequiresDependency(requiresDependency = CustomAnnotationsBase.Dependencies.Picasso)
    public static <T extends ImageView> void setCircularImageWithPicassoNoCache(String urlThumbnail,
                                                                                final T viewToSet,
                                                                                final int backupImageResourceId,
                                                                                final Integer circularFrameColor,
                                                                                final Integer circularFrameWidth) {
        if (StringUtilities.isNullOrEmpty(urlThumbnail)) {
            try {
                Picasso.get().load(backupImageResourceId).
                        transform(new CircleTransform()).into(viewToSet);

            } catch (Exception e) {
            }
        } else {

            final String innerUrlThumbnail = urlThumbnail;

            try {
                Picasso.get()
                        .load(innerUrlThumbnail)
                        .transform(new CircleTransform())
                        .into(viewToSet);
            } catch (Exception e) {
                try {
                    Picasso.get().load(backupImageResourceId).
                            transform(new CircleTransform()).into(viewToSet);
                } catch (Exception e1) {
                }
            }
        }
    }


    /**
     * Set a circular image into a view and set caching. Overloaded to allow for excluding
     * max cache size float
     *
     * @param urlThumbnail          URL String to use
     * @param viewToSet             View to set it into
     * @param backupImageResourceId Backup resource id in case the String url fails parsing
     * @param <T>                   {T extends View}
     */
    @CustomAnnotationsBase.RequiresDependency(requiresDependency = CustomAnnotationsBase.Dependencies.Picasso)
    public static <T extends ImageView> void setCircularImageWithPicassoNoCache(String urlThumbnail,
                                                                                final T viewToSet,
                                                                                final int backupImageResourceId) {
        setCircularImageWithPicassoNoCache(urlThumbnail, viewToSet, backupImageResourceId,
                null, null);
    }

    //endregion

    //region Picasso Images With Caching
    /**
     * Set an image into a view and set caching.
     *
     * @param urlThumbnail          URL String to use
     * @param viewToSet             View to set it into
     * @param backupImageResourceId Backup resource id in case the String url fails parsing
     * @param <T>                   {T extends View}
     */
    @CustomAnnotationsBase.RequiresDependency(requiresDependency = CustomAnnotationsBase.Dependencies.Picasso)
    public static <T extends ImageView> void setImageWithPicasso(String urlThumbnail,
                                                                 final T viewToSet,
                                                                 final int backupImageResourceId) {
        if (urlThumbnail == null) {
            urlThumbnail = "";
        }
        if (urlThumbnail.isEmpty() || urlThumbnail.equalsIgnoreCase("")) {
            viewToSet.setImageResource(backupImageResourceId);
        } else {
            final String innerUrlThumbnail = urlThumbnail;

            try {
                Picasso.get()
                        .load(urlThumbnail)
                        .networkPolicy(NetworkPolicy.OFFLINE)
                        .into(viewToSet, new Callback() {
                            @Override
                            public void onSuccess() {
                                //Nothing, image is set
                            }

                            @Override
                            public void onError(Exception e) {
                                //Can trigger if image not in cache
                                Picasso.get().load(innerUrlThumbnail)
                                        .into(viewToSet, new Callback() {
                                            @Override
                                            public void onSuccess() {
                                                try {
                                                    List<String> toCache = new ArrayList<String>();
                                                    toCache.add(innerUrlThumbnail);
                                                    ImageUtilities.LoadImagesIntoPicassoCache async = new
                                                            ImageUtilities.LoadImagesIntoPicassoCache(toCache);
                                                    async.execute();
                                                } catch (Exception e2) {}
                                            }

                                            @Override
                                            public void onError(Exception e) {
                                                Picasso.get()
                                                        .load(backupImageResourceId)
                                                        .into(viewToSet);
                                            }
                                        });
                                //Load the image into cache for next time

                            }
                        });
            } catch (Exception e) {
                //L.m("catch caught in picasso setImageWithPicasso call");
                viewToSet.setImageResource(backupImageResourceId);
            }

        }
    }

    //endregion

    //region Picasso Images Without Caching
    /**
     * Set an image into a view and set caching. Overloaded to allow for excluding
     * max cache size float
     *
     * @param urlThumbnail          URL String to use
     * @param viewToSet             View to set it into
     * @param backupImageResourceId Backup resource id in case the String url fails parsing
     * @param <T>                   {T extends View}
     */
    @CustomAnnotationsBase.RequiresDependency(requiresDependency = CustomAnnotationsBase.Dependencies.Picasso)
    public static <T extends ImageView> void setImageWithPicassoNoCache(String urlThumbnail,
                                                                        final T viewToSet,
                                                                        final int backupImageResourceId) {
        if (urlThumbnail == null) {
            urlThumbnail = "";
        }
        if (urlThumbnail.isEmpty() || urlThumbnail.equalsIgnoreCase("")) {
            viewToSet.setImageResource(backupImageResourceId);
        } else {
            final String innerUrlThumbnail = urlThumbnail;
            try {
                Picasso.get()
                        .load(urlThumbnail)
                        .into(viewToSet);
            } catch (Exception e) {
                //L.m("catch caught in picasso setImageWithPicasso call");
                viewToSet.setImageResource(backupImageResourceId);
            }

        }
    }

    //endregion

    //region Picasso Remove Images from cache Async Method
    /**
     * Remove images from the Picasso cache. This runs on a background thread and does not return anything
     * @param imageUrlsToRemove
     */
    @CustomAnnotationsBase.RequiresDependency(requiresDependency = CustomAnnotationsBase.Dependencies.Picasso)
    public static void removeImageFromPicassoCache(List<String> imageUrlsToRemove){
        try {
            RemoveImagesFromPicassoCache async = new RemoveImagesFromPicassoCache(imageUrlsToRemove);
            async.execute();
        } catch (Exception e){
            L.m("Could not remove images from Picasso cache: " + e.getMessage());
        }
    }

    /**
     * Removes images from the picasso cache. Reference:
     * http://www.zoftino.com/android-picasso-image-downloading-and-caching-library-tutorial
     */
    @CustomAnnotationsBase.RequiresDependency(requiresDependency = CustomAnnotationsBase.Dependencies.Picasso)
    public static class RemoveImagesFromPicassoCache extends AsyncTask<Void, Void, Void> {
        private List<String> imageURLs;

        /**
         * Load Images into cache constructor
         *
         * @param imageURLs A list of the image URLs to set
         */
        public RemoveImagesFromPicassoCache(List<String> imageURLs) {
            this.imageURLs = imageURLs;
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (String str : imageURLs) {
                try {
                    if (isCancelled()) {
                        return null;
                    }
                    Picasso.get().invalidate(str);
                    Thread.sleep(10);
                } catch (OutOfMemoryError e1) {
                    Picasso.get().invalidate(str);
                    return null;
                } catch (Exception e) {
                }
            }

            return null;
        }
    }

    //endregion

    //region Picasso Add / Load Images into the cache Async Method
    /**
     * Loads images into the picasso cache by using the fetch() call. Reference:
     * http://stackoverflow.com/questions/23978828/how-do-i-use-disk-caching-in-picasso
     */
    @CustomAnnotationsBase.RequiresDependency(requiresDependency = CustomAnnotationsBase.Dependencies.Picasso)
    public static class LoadImagesIntoPicassoCache extends AsyncTask<Void, Void, Void> {
        private List<String> imageURLs;

        /**
         * Load Images into cache constructor
         *
         * @param imageURLs A list of the image URLs to set
         */
        public LoadImagesIntoPicassoCache(List<String> imageURLs) {
            this.imageURLs = imageURLs;
        }

        @Override
        protected Void doInBackground(Void... params) {
            for (String str : imageURLs) {
                try {
                    if (isCancelled()) {
                        return null;
                    }

                    Picasso.get().load(str).fetch();
                    Thread.sleep(20);
                } catch (OutOfMemoryError e1) {
                    //If we run out of memory, make sure to catch it!
                    Picasso.get().invalidate(str);
                    return null;
                } catch (Exception e) {
                }
            }

            return null;
        }
    }

    //endregion

    //region Bitmap Operations

    /**
     * Adjust the photo orientation
     *
     * @param pathToFile
     * @return
     */
    public static Bitmap adjustPhotoOrientation(String pathToFile) {
        try {
            Bitmap bitmap = BitmapFactory.decodeFile(pathToFile);
            ExifInterface exif = new ExifInterface(pathToFile);
            int exifOrientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
            int rotate = 0;

            switch (exifOrientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
            }
            if (rotate != 0) {
                int w = bitmap.getWidth();
                int h = bitmap.getHeight();

                // Setting pre rotateImage
                Matrix mtx = new Matrix();
                mtx.preRotate(rotate);

                // Rotating Bitmap & convert to ARGB_8888, required by tess
                bitmap = Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, false);
                bitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
                return bitmap;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Rotates an image 90 degrees counter-clockwise
     *
     * @param bitmap Bitmap to rotateImage
     * @return
     */
    public static Bitmap rotateImageCounterClockwise(@NonNull Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        return rotateImage(bitmap, 270);
    }

    /**
     * Rotates an image 90 degrees clockwise
     *
     * @param bitmap Bitmap to rotateImage
     * @return
     */
    public static Bitmap rotateImageClockwise(@NonNull Bitmap bitmap) {
        if (bitmap == null) {
            return null;
        }
        return rotateImage(bitmap, 90);
    }

    /**
     * For rotating images
     *
     * @param bitmap Bitmap to rotateImage
     * @param degree Degree amount to rotateImage. If <0 or >360, will set to 90 degrees.
     *               (Range is 0-360)
     * @return
     */
    public static Bitmap rotateImage(@NonNull Bitmap bitmap,
                                     @IntRange(from = 0, to = 360) int degree) {
        if (bitmap == null) {
            return null;
        }
        //Shouldn't be possible, but, you know... reasons.
        degree = (degree < 0 || degree > 360) ? 90 : degree;
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        Matrix mtx = new Matrix();
        mtx.setRotate(degree);

        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }

    /**
     * Converts an inputstream to a byte array (Mostly useful for sending images via JSON)
     *
     * @param is Input stream, if using a URI, open it by calling:
     *           InputStream iStream =   context.getContentResolver().openInputStream(uri);
     * @return Byte Array
     */
    public static Bitmap convertISToBitmap(InputStream is) {
        try {
            return BitmapFactory.decodeStream(is);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Convert a Bitmap into a byte Array
     *
     * @param bitmap
     * @return
     */
    public static byte[] convertBitmapToByte(Bitmap bitmap) {
        try {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            return byteArray;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Combines multiple bitmaps together into one
     *
     * @param aParts An array of bitmaps
     * @return Returns a bitmap image
     */
    public static Bitmap combineBitmaps(Bitmap[] aParts) {
        Bitmap[] parts = aParts;
        Bitmap result = Bitmap.createBitmap(parts[0].getWidth() * 2, parts[0].getHeight() * 2, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(result);
        Paint paint = new Paint();
        for (int i = 0; i < parts.length; i++) {
            canvas.drawBitmap(parts[i], parts[i].getWidth() * (i % 2), parts[i].getHeight() * (i / 2), paint);
        }
        return result;
    }

    /**
     * Decode a bitmap from a resource
     *
     * @param res
     * @param resId
     * @param reqWidth
     * @param reqHeight
     * @return
     */
    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static String convertBitmapToBase64String(@NonNull Bitmap bitmap){
        return ImageUtilities.convertBitmapToBase64String(bitmap, ImageMimeType.PNG);
    }

    public static String convertBitmapToBase64String(@NonNull Bitmap bitmap, @Nullable ImageMimeType mimeType){
        if(bitmap == null){
            return null;
        }
        try {
            if(mimeType == null){
                mimeType = ImageMimeType.PNG;
            }
            Bitmap.CompressFormat c;
            switch (mimeType){
                case JPEG:
                    c = Bitmap.CompressFormat.JPEG;
                    break;
                default:
                case GIF:
                case UNKNOWN:
                case PNG:
                    c = Bitmap.CompressFormat.PNG;
                    break;
            }
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(c, 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
            return Base64.encodeToString(byteArray, Base64.DEFAULT);
        } catch (Exception e){
            return null;
        }
    }

    public static String convertBitmapToBase64String(@NonNull Bitmap bitmap, @Nullable Bitmap.CompressFormat compressFormat){
        if(bitmap == null){
            return null;
        }
        try {
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(((compressFormat == null) ? Bitmap.CompressFormat.PNG : compressFormat), 100, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
            return Base64.encodeToString(byteArray, Base64.DEFAULT);
        } catch (Exception e){
            return null;
        }
    }

    /**
     * Convert a base64String into a bitmap
     * @param base64Image Base64 String
     * @return {@link Bitmap}
     */
    public static Bitmap convertBase64StringToBitmap(@NonNull String base64Image){
        byte[] decodedString = Base64.decode(base64Image, Base64.DEFAULT);
        Bitmap bitmap;
        try {
            bitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            if(bitmap == null){
                throw new Exception();
            }
            return bitmap;
        } catch (Exception e){
            e.printStackTrace();
            try {
                final String pureBase64Encoded = base64Image.substring(base64Image.indexOf(",")  + 1);
                byte[] decodedString1 = Base64.decode(pureBase64Encoded, Base64.DEFAULT);
                bitmap = BitmapFactory.decodeByteArray(decodedString1, 0, decodedString.length);
                return bitmap;
            } catch (Exception ee){
                ee.printStackTrace();
            }
        }
        return null;
    }

    /**
     * Download a file from the web. This defaults to a photo (png)
     */
    public static class DownloadImageFromWeb extends AsyncTask<Void, Integer, File> {

        private Context context;
        private String imageUrl;
        private OnTaskCompleteListener listener;
        private ProgressDialog dialog;
        private long lengthOfTimeToDelay, startTime, endTime;

        /**
         * Download an image from the web into a file and send that file back via the listener
         *
         * @param context
         * @param imageUrl            String image Url
         * @param dialog              Progress dialog to show
         * @param listener            listener to send data back on
         * @param lengthOfTimeToDelay Length of time to 'delay' in that if the time to download
         *                            the file is shorter than this, it will add time on the
         *                            progress dialog to allow time to finish. This is best used
         *                            when you have a "download animation" that you want seen, your
         *                            internet is too fast, and you don't see the animation.
         */
        public DownloadImageFromWeb(Context context, String imageUrl, ProgressDialog dialog,
                                    OnTaskCompleteListener listener, Long lengthOfTimeToDelay) {
            this.context = context;
            this.imageUrl = imageUrl;
            this.listener = listener;
            this.dialog = dialog;
            if (this.dialog == null) {
                //Removed on 2017-07-05 Due to problems with compiling
                //this.dialog = PGMacCustomProgressBar.buildElasticDialog(context);
                this.dialog = new ProgressDialog(context);
            }
            if (lengthOfTimeToDelay == null) {
                lengthOfTimeToDelay = (long) (PGMacTipsConstants.ONE_SECOND * 2.6);
            }
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.dialog.show();
        }

        @Override
        protected File doInBackground(Void... params) {
            File file = FileUtilities.generateFileForImage(context, null, null);
            int count;
            this.startTime = DateUtilities.getCurrentDateLong();
            try {
                URL url = new URL(imageUrl);
                URLConnection conexion = url.openConnection();
                conexion.connect();
                int lenghtOfFile = conexion.getContentLength();
                InputStream input = new BufferedInputStream(url.openStream());
                OutputStream output = new FileOutputStream(file);
                byte data[] = new byte[1024];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    int publishNum = ((int) ((total * 100 / lenghtOfFile)));
                    publishProgress(publishNum);
                    output.write(data, 0, count);
                }
                output.flush();
                output.close();
                input.close();
                return file;
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            if (values[0] != null) {
                int prog = values[0];
                if (prog % 10 == 0) {
                    if (prog == 100) {
                        prog = 99;
                    }
                    dialog.setProgress(prog);
                }
            }
        }

        @Override
        protected void onPostExecute(File file) {
            super.onPostExecute(file);
            final File file1 = file;
            this.endTime = DateUtilities.getCurrentDateLong();
            long totalTime = (endTime - startTime);
            long timeLeft = lengthOfTimeToDelay - totalTime;
            Handler handler = new Handler();

            if (file == null) {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            dialog.dismiss();
                        } catch (Exception e) {
                        }
                    }
                }, PGMacTipsConstants.ONE_SECOND);
                listener.onTaskComplete("Url Error", PGMacTipsConstants.TAG_PHOTO_BAD_URL);

            } else {
                dialog.setProgress(100);
                if (timeLeft <= 0) {
                    try {
                        dialog.dismiss();
                        listener.onTaskComplete(file1, PGMacTipsConstants.TAG_FILE_DOWNLOADED);
                    } catch (Exception e) {
                    }
                } else {
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                dialog.dismiss();
                                listener.onTaskComplete(file1, PGMacTipsConstants.TAG_FILE_DOWNLOADED);
                            } catch (Exception e) {
                            }
                        }
                    }, (int) timeLeft);
                }
            }
        }
    }

    /**
     * Determine if a bitmap is too large as compared to passed param
     * @param bmp Bitmap to check
     * @param desiredSizeInBytes Desired size (in Bytes) to check against
     * @return boolean, if true, bitmap is larger than the desired size, else, it is not.
     */
    public static boolean isImageTooLarge(@NonNull Bitmap bmp, long desiredSizeInBytes){
        long bitmapSize = (bmp.getRowBytes() * bmp.getHeight());
        float shrinkFactor = desiredSizeInBytes / bitmapSize;
        if(shrinkFactor >= 1){
            return false;
        } else {
            return true;
        }
    }

    /**
     * Determine the float value needed to resize the image so that it is less in size (Bytes)
     * than the value passed
     * @param bmp Bitmap to check
     * @param desiredSizeInBytes Desired size in bytes of the image
     * @return float value to resize. IE, if 0.34 is returned, the bitmap in question needs
     *         to be shrunk down by 34% to reach the desired size
     */
    public static float getImageResizeFactor(@NonNull Bitmap bmp, long desiredSizeInBytes){
        long bitmapSize = (bmp.getRowBytes() * bmp.getHeight());
        return (desiredSizeInBytes / bitmapSize);
    }

    /**
     * Resize a photo
     * @param bmp Bitmap to resize
     * @param factorToDivide Factor to divide by. if (IE) 2 is passed, it will cut the
     *                       image in half, 10 will cut it down 10x in size. Note that
     *                       scaling too much will result in geometric size jumps.
     * @return Resized bitmap. If it fails, will send back original
     */
    public static Bitmap resizePhoto(@NonNull Bitmap bmp, int factorToDivide){
        if(factorToDivide <= 1){
            factorToDivide = 2;
        }
        try {
            return Bitmap.createScaledBitmap(bmp, (int)(bmp.getWidth() / factorToDivide),
                    (int)(bmp.getHeight() / factorToDivide), true);
        } catch (Exception e){
            return bmp;
        }
    }

    /**
     * Resize a photo
     * @param bmp Bitmap to resize
     * @param desiredImageSizeInBytes The desired image size in bytes. IE, sending 5000000
     *                                (5 Million) would be a 5 Megapixel (MP) image.
     * @return Resized bitmap. If it fails, will send back original
     */
    public static Bitmap resizePhoto(@NonNull Bitmap bmp, long desiredImageSizeInBytes){
        try {
            double flt = (double) desiredImageSizeInBytes;
            double height = Math.sqrt(flt /
                    (((double) bmp.getWidth()) / bmp.getHeight()));
            double width = (height / bmp.getHeight()) * bmp.getWidth();
            return Bitmap.createScaledBitmap(bmp, (int)(width),
                    (int)(height), true);
        } catch (Exception e){
            e.printStackTrace();
            return bmp;
        }
    }

    /**
     * Decode a file from the path and return a bitmap
     *
     * @param uri
     * @param context
     * @return
     */
    public static Bitmap decodeFileFromPath(Uri uri, Context context) {

        InputStream in = null;
        try {
            in = context.getContentResolver().openInputStream(uri);

            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;

            BitmapFactory.decodeStream(in, null, o);
            in.close();


            int scale = 1;
            int inSampleSize = 1024;
            if (o.outHeight > inSampleSize || o.outWidth > inSampleSize) {
                scale = (int) Math.pow(2, (int) Math.round(Math.log(inSampleSize / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
            }

            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            in = context.getContentResolver().openInputStream(uri);
            Bitmap b = BitmapFactory.decodeStream(in, null, o2);
            in.close();

            return b;

        } catch (FileNotFoundException e) {
            //e.printStackTrace();
        } catch (IOException e) {
            //e.printStackTrace();
        }
        return null;
    }

    //endregion

    //region Drawable Operations
    /**
     * Set the drawable to a specific color and return it
     *
     * @param drawable   The drawable to change
     * @param colorToSet The color to set it to
     * @return Drawable
     * @throws NullPointerException, if it fails, throws a null pointer
     */
    public static Drawable colorDrawable(@NonNull Drawable drawable, int colorToSet) {
        try {
            drawable.mutate().setColorFilter(colorToSet, PorterDuff.Mode.MULTIPLY);
            return drawable;
        } catch (Exception e) {
            e.printStackTrace();
            return drawable;
        }
    }

    /**
     * Set the drawable to a specific color and return it
     *
     * @param drawableId the int ID of the drawable to change
     * @param colorToSet The color to set it to
     * @return Drawable
     * @throws NullPointerException, if it fails, throws a null pointer
     */
    public static Drawable colorDrawable(int drawableId, int colorToSet, Context context) {
        try {
            Drawable drawable = ContextCompat.getDrawable(context, drawableId);
            drawable.mutate().setColorFilter(colorToSet, PorterDuff.Mode.MULTIPLY);
            return drawable;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //endregion

    //region Misc Operations

    /**
     * Convert an image to a byte array
     *
     * @param uri
     * @param context
     * @return
     */
    public static byte[] convertImageToByte(Uri uri, Context context) {
        byte[] data = null;
        try {
            ContentResolver cr = context.getContentResolver();
            InputStream inputStream = cr.openInputStream(uri);
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
            data = baos.toByteArray();
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (Exception e2) {
                }
            }
            if (baos != null) {
                try {
                    baos.close();
                } catch (Exception e2) {
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return data;
    }


    /**
     * TBD in the future
     */
    private static void zoomAView() {
        //https://developer.android.com/training/animation/zoom.html
    }

    //endregion

    ///////////////////////////////////////////
    //Base64 String Image --> String Encoding//
    ///////////////////////////////////////////

    //region Base64 Encoding

    /**
     * Encode a Bitmap to a base 64 String
     *
     * @param bm The Bitmap to convert
     * @return Base 64 Encoded String
     */
    public static String encodeImage(Bitmap bm) {
        if (bm == null) {
            return null;
        }

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] b = baos.toByteArray();
            String encImage = Base64.encodeToString(b, Base64.DEFAULT);
            return encImage;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Encode an image on a background thread
     *
     * @param listener {@link OnTaskCompleteListener}
     * @param bm       Bitmap to convert
     */
    public static void encodeImage(@NonNull final OnTaskCompleteListener listener,
                                   final Bitmap bm) {
        if (bm == null) {
            listener.onTaskComplete(null, PGMacTipsConstants.TAG_BASE64_IMAGE_ENCODE_FAIL);
            return;
        }
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... voids) {
                try {
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] b = baos.toByteArray();
                    String encImage = Base64.encodeToString(b, Base64.DEFAULT);
                    return encImage;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String str) {
                if (!StringUtilities.isNullOrEmpty(str)) {
                    listener.onTaskComplete(str,
                            PGMacTipsConstants.TAG_BASE64_IMAGE_ENCODE_SUCCESS);
                } else {
                    listener.onTaskComplete(null,
                            PGMacTipsConstants.TAG_BASE64_IMAGE_ENCODE_FAIL);
                }
            }
        }.execute();
    }

    /**
     * Encode an image to a base 64 String
     *
     * @param file The image File to convert
     * @return Base 64 Encoded String
     */
    public static String encodeImage(File file) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            Bitmap bm = BitmapFactory.decodeStream(fis);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            if (bm == null) {
                return null;
            }
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] b = baos.toByteArray();
            String encImage = Base64.encodeToString(b, Base64.DEFAULT);
            //Base64.de
            return encImage;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    /**
     * Encode an image to a base64 String on a background thread
     *
     * @param listener {@link OnTaskCompleteListener}
     * @param file     The File to convert
     */
    public static void encodeImage(@NonNull final OnTaskCompleteListener listener,
                                   final File file) {
        if (file == null) {
            listener.onTaskComplete(null, PGMacTipsConstants.TAG_BASE64_IMAGE_ENCODE_FAIL);
            return;
        }
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... voids) {
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(file);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                try {
                    Bitmap bm = BitmapFactory.decodeStream(fis);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    if (bm == null) {
                        return null;
                    }
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] b = baos.toByteArray();
                    String encImage = Base64.encodeToString(b, Base64.DEFAULT);
                    //Base64.de
                    return encImage;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String str) {
                if (!StringUtilities.isNullOrEmpty(str)) {
                    listener.onTaskComplete(str,
                            PGMacTipsConstants.TAG_BASE64_IMAGE_ENCODE_SUCCESS);
                } else {
                    listener.onTaskComplete(null,
                            PGMacTipsConstants.TAG_BASE64_IMAGE_ENCODE_FAIL);
                }
            }
        }.execute();
    }

    /**
     * Encode an image to a base 64 String
     *
     * @param path The path String to the image file
     * @return Base 64 Encoded String
     */
    public static String encodeImage(String path) {
        File imagefile = new File(path);
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(imagefile);
            Bitmap bm = BitmapFactory.decodeStream(fis);
            if (bm == null) {
                return null;
            }
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] b = baos.toByteArray();
            String encImage = Base64.encodeToString(b, Base64.DEFAULT);
            return encImage;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;

    }

    /**
     * Encode an image to a base 64 String on a background thread
     *
     * @param listener {@link OnTaskCompleteListener}
     * @param path     The Path String to the image
     */
    public static void encodeImage(@NonNull final OnTaskCompleteListener listener,
                                   final String path) {
        if (StringUtilities.isNullOrEmpty(path)) {
            listener.onTaskComplete(null, PGMacTipsConstants.TAG_BASE64_IMAGE_ENCODE_FAIL);
            return;
        }
        new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... voids) {
                File imagefile = new File(path);
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(imagefile);
                    Bitmap bm = BitmapFactory.decodeStream(fis);
                    if (bm == null) {
                        return null;
                    }
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    byte[] b = baos.toByteArray();
                    String encImage = Base64.encodeToString(b, Base64.DEFAULT);
                    return encImage;
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(String str) {
                if (!StringUtilities.isNullOrEmpty(str)) {
                    listener.onTaskComplete(str,
                            PGMacTipsConstants.TAG_BASE64_IMAGE_ENCODE_SUCCESS);
                } else {
                    listener.onTaskComplete(null,
                            PGMacTipsConstants.TAG_BASE64_IMAGE_ENCODE_FAIL);
                }
            }
        }.execute();
    }

    /**
     * Determine the Mime Type of an image from the base64 String
     * @param bitmap Bitmap image to determine type from
     * @return {@link ImageMimeType}
     */
    public static ImageMimeType determineImageMimeType(@NonNull Bitmap bitmap){
        if(bitmap == null){
            return ImageMimeType.UNKNOWN;
        }
        try {
            return ImageUtilities.determineImageMimeType(ImageUtilities.convertBitmapToBase64String(bitmap));
        } catch (Exception e){
            return ImageMimeType.UNKNOWN;
        }
    }

    /**
     * Determine the Mime Type of an image from the base64 String
     * @param base64Image Base64 String of an image
     * @return {@link ImageMimeType}
     */
    public static ImageMimeType determineImageMimeType(@NonNull String base64Image){
        if(StringUtilities.isNullOrEmpty(base64Image)){
            return ImageMimeType.UNKNOWN;
        }
        if(base64Image.charAt(0)=='/'){
            return ImageMimeType.JPEG;
        }else if(base64Image.charAt(0)=='R'){
            return ImageMimeType.GIF;
        }else if(base64Image.charAt(0)=='i'){
            return ImageMimeType.PNG;
        } else {
            return ImageMimeType.UNKNOWN;
        }
    }

    //endregion

    //region Image Getter Utilities

    /**
     * Get the Left, Top, Right, and Bottom sides of an image.
     * @param topLeftX topLeft X position (from x,y coordinates)
     * @param topLeftY topLeft Y position (from x,y coordinates)
     * @param bottomLeftX bottomLeft X position (from x,y coordinates)
     * @param bottomLeftY bottomRight Y position (from x,y coordinates)
     * @param topRightX topRight X position (from x,y coordinates)
     * @param topRightY topRight Y position (from x,y coordinates)
     * @param bottomRightX bottomRight X position (from x,y coordinates)
     * @param bottomRightY bottomRight Y position (from x,y coordinates)
     * @return
     */
    public static int[] getLTRBSidesOfImage(int topLeftX, int topLeftY,
                                            int bottomLeftX, int bottomLeftY,
                                            int topRightX, int topRightY,
                                            int bottomRightX, int bottomRightY){
        if(topLeftX < 0 || topRightX < 0 || topLeftY < 0 || topRightY < 0 || bottomLeftX < 0 ||
                bottomLeftY < 0 || bottomRightX < 0 || bottomRightY < 0) {
            L.m(INVALID_PIXEL_POSITIONS);
            return null;
        }
        return new int[]{
                ImageUtilities.getLeftSideXCoord(topLeftX, bottomLeftX),
                ImageUtilities.getTopSideYCoord(topLeftY, topRightY),
                ImageUtilities.getRightSideXCoord(topRightX, bottomRightX),
                ImageUtilities.getBottomSideYCoord(bottomLeftY, bottomRightY)
        };
    }

    private static int getLeftSideXCoord(int topLeftX, int bottomLeftX){
        return (topLeftX < bottomLeftX) ? topLeftX : bottomLeftX;
    }

    private static int getRightSideXCoord(int topRightX, int bottomRightX){
        return (topRightX > bottomRightX) ? topRightX : bottomRightX;
    }

    private static int getTopSideYCoord(int topLeftY, int topRightY){
        return (topLeftY < topRightY) ? topLeftY : topRightY;
    }

    private static int getBottomSideYCoord(int bottomLeftY, int bottomRightY){
        return (bottomLeftY > bottomRightY) ? bottomLeftY : bottomRightY;
    }

    //endregion

    //region Image Cropping

    /**
     * Crop an image to the positions passed. If any # <0 is passed or an error occurs, it will
     * return the original bitmap passed
     * @param imageToBeCropped Image to be cropped
     * @param topLeftX topLeft X position (from x,y coordinates)
     * @param topLeftY topLeft Y position (from x,y coordinates)
     * @param bottomLeftX bottomLeft X position (from x,y coordinates)
     * @param bottomLeftY bottomRight Y position (from x,y coordinates)
     * @param topRightX topRight X position (from x,y coordinates)
     * @param topRightY topRight Y position (from x,y coordinates)
     * @param bottomRightX bottomRight X position (from x,y coordinates)
     * @param bottomRightY bottomRight Y position (from x,y coordinates)
     * @return Cropped image. Will return same passed bitmap if operation fails
     */
    public static Bitmap cropImage(@NonNull Bitmap imageToBeCropped,
                                   int topLeftX, int topLeftY,
                                   int bottomLeftX, int bottomLeftY,
                                   int topRightX, int topRightY,
                                   int bottomRightX, int bottomRightY){
        if(topLeftX < 0 || topRightX < 0 || topLeftY < 0 || topRightY < 0 || bottomLeftX < 0 ||
                bottomLeftY < 0 || bottomRightX < 0 || bottomRightY < 0){
            L.m(INVALID_PIXEL_POSITIONS);
            return imageToBeCropped;
        }

        return cropImage(imageToBeCropped, ImageUtilities.getLeftSideXCoord(topLeftX, bottomLeftX),
                ImageUtilities.getTopSideYCoord(topLeftY, topRightY),
                ImageUtilities.getRightSideXCoord(topRightX, bottomRightX),
                ImageUtilities.getBottomSideYCoord(bottomLeftY, bottomRightY));
    }

    /**
     * Crop an image to the positions passed. If any # <0 is passed or an error occurs, it will
     * return the original bitmap passed
     * @param imageToBeCropped Image to be cropped
     * @param topLeftX topLeft X position (from x,y coordinates)
     * @param topLeftY topLeft Y position (from x,y coordinates)
     * @param bottomLeftX bottomLeft X position (from x,y coordinates)
     * @param bottomLeftY bottomRight Y position (from x,y coordinates)
     * @param topRightX topRight X position (from x,y coordinates)
     * @param topRightY topRight Y position (from x,y coordinates)
     * @param bottomRightX bottomRight X position (from x,y coordinates)
     * @param bottomRightY bottomRight Y position (from x,y coordinates)
     * @param sidesBufferPercent Float percentage to allow buffer on sides for cropping. IE, sending
     *                           in 0.10 would add a 10% buffer on each side to 'zoom out' on
     *                           the cropped image. Useful if you want some extra side room
     * @return Cropped image. Will return same passed bitmap if operation fails
     */
    public static Bitmap cropImage(@NonNull Bitmap imageToBeCropped,
                                   int topLeftX, int topLeftY,
                                   int bottomLeftX, int bottomLeftY,
                                   int topRightX, int topRightY,
                                   int bottomRightX, int bottomRightY,
                                   @Nullable Float sidesBufferPercent){
        if(topLeftX < 0 || topRightX < 0 || topLeftY < 0 || topRightY < 0 || bottomLeftX < 0 ||
                bottomLeftY < 0 || bottomRightX < 0 || bottomRightY < 0){
            L.m(INVALID_PIXEL_POSITIONS);
            return imageToBeCropped;
        }

        return cropImage(imageToBeCropped, ImageUtilities.getLeftSideXCoord(topLeftX, bottomLeftX),
                ImageUtilities.getTopSideYCoord(topLeftY, topRightY),
                ImageUtilities.getRightSideXCoord(topRightX, bottomRightX),
                ImageUtilities.getBottomSideYCoord(bottomLeftY, bottomRightY), sidesBufferPercent);
    }

    /**
     * Crop an image to the positions passed. If any # <0 is passed or an error occurs, it will
     * return the original bitmap passed
     * @param imageToBeCropped Image to be cropped
     * @param startingXPos Starting X position (left side of the image)
     * @param startingYPos Starting Y position (Top side of the image)
     * @param endingXPos Ending X position (Right side of the image)
     * @param endingYPos Ending Y position (Bottom side of the image)
     * @return Cropped image. Will return same passed bitmap if operation fails
     */
    public static Bitmap cropImage(@NonNull Bitmap imageToBeCropped,
                                   int startingXPos, int startingYPos,
                                   int endingXPos, int endingYPos){
        if(startingYPos < 0 || startingXPos < 0 || endingYPos < 0 || endingXPos < 0){
            L.m(INVALID_PIXEL_POSITIONS);
            return imageToBeCropped;
        }
        try {
            return Bitmap.createBitmap(imageToBeCropped, startingXPos, startingYPos,
                    (endingXPos - startingXPos), (endingYPos - startingYPos));
        } catch (IllegalArgumentException ile){
            ile.printStackTrace();
        } catch (Exception e){
            e.printStackTrace();
        }
        return imageToBeCropped;
    }

    /**
     * Crop an image to the positions passed. If any # <0 is passed or an error occurs, it will
     * return the original bitmap passed
     * @param imageToBeCropped Image to be cropped
     * @param startingXPos Starting X position (left side of the image)
     * @param startingYPos Starting Y position (Top side of the image)
     * @param endingXPos Ending X position (Right side of the image)
     * @param endingYPos Ending Y position (Bottom side of the image)
     * @param sidesBufferPercent Float percentage to allow buffer on sides for cropping. IE, sending
     *                           in 0.10 would add a 10% buffer on each side to 'zoom out' on
     *                           the cropped image. Useful if you want some extra side room
     * @return Cropped image. Will return same passed bitmap if operation fails
     */
    public static Bitmap cropImage(@NonNull Bitmap imageToBeCropped,
                                   int startingXPos, int startingYPos,
                                   int endingXPos, int endingYPos,
                                   @Nullable Float sidesBufferPercent){
        if(startingYPos < 0 || startingXPos < 0 || endingYPos < 0 || endingXPos < 0){
            L.m(INVALID_PIXEL_POSITIONS);
            return imageToBeCropped;
        }
        if(sidesBufferPercent == null){
            sidesBufferPercent = 0F;
        }
        if(sidesBufferPercent < 0 || sidesBufferPercent >= 1){
            sidesBufferPercent = 0F;
        }
        startingXPos = (int)(startingXPos - (startingXPos * sidesBufferPercent));
        endingXPos = (int)(endingXPos + (endingXPos * sidesBufferPercent));
        startingYPos = (int)(startingYPos - (startingYPos * sidesBufferPercent));
        endingYPos = (int)(endingYPos + (endingYPos * sidesBufferPercent));

        startingXPos = (startingXPos < 0) ? 0 : startingXPos;
        startingYPos = (startingYPos < 0) ? 0 : startingYPos;
        endingXPos = (endingXPos > imageToBeCropped.getWidth()) ?
                imageToBeCropped.getWidth() : endingXPos;
        endingYPos = (endingYPos > imageToBeCropped.getHeight()) ?
                imageToBeCropped.getHeight() : endingYPos;

        return ImageUtilities.cropImage(imageToBeCropped,
                startingXPos, startingYPos, endingXPos, endingYPos);
    }

    //endregion
}
